import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UpdateProfile = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [message, setMessage] = useState('');

  const handleUpdateProfile = async (e) => {
    //e.preventDefault();

    const token = localStorage.getItem('token');

    try {
      const response = await fetch('http://localhost:4000/users/profile', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          mobileNo: mobileNo
        }),
      });

      if (response.ok) {
        //setMessage('Profile updated successfully');
        Swal.fire({
            icon: 'success',
            title: 'Profile updated successfully',
            showConfirmButton: false
        })

        setFirstName('');
        setLastName('');
        setMobileNo('');
      } else {
        const errorData = await response.json();
        console.log(errorData.message);

        Swal.fire({
            title: "An error occurred. Please try again.",
            icon: "error"
        })
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  return (
    <Container>
      <Row>
        <Col xs={12} lg={12} className="mx-auto">
          <h2>Update Profile</h2>
          <Form className="mt-3" onSubmit={handleUpdateProfile}>
            <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mt-3" controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mt-3" controlId="mobileNo">
              <Form.Label>Mobile No</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setMobileNo(e.target.value)}
                required
              />
            </Form.Group>

            {message && <p className="text-danger">{message}</p>}

            <Button className="mt-4" variant="primary" type="submit">
              Update Profile
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default UpdateProfile;
