console.log("hello fucker");

//Array Methods
	//JS has built-in functions and methods for arrays. This allows us to manipulate and access array items

	//Mutator Methods
	/*
		are functions that mutate or change an array after they are created
		these methods manipulate the original array performing various tasks such as adding and also removing elements
	*/

//array
let fruits = ["apple", "orange", "kiwi", "dragon fruit"];
console.log(fruits);
console.log(typeof fruits);

//push()
//adds an element in the END of an array and returns the array's length.

//syntax:
	//arrayName.push("ItemToPush");
	
	console.log("Current array: ");
	console.log(fruits);

	//if we assign it to a variable, we are able to save the length of the array
	let fruitsLength = fruits.push("mango");
	console.log(fruitsLength);
	console.log("mutated array from push method: ");
	console.log(fruits);

//pop()
//removes an element in the END of an array and returns the element removed.

//Syntax
	//arrayName.pop();

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method: ");
	console.log(fruits);

//MA

	function removeFruit(){
		let prutas = fruits.pop();
		console.log(prutas);
	}

//unshift()
//adds one or more elements at the beginning of an array
//Syntax
	// -arrayName.unshift("elementA");
	// -arrayName.unshift("elementA", "elementB");

	fruits.unshift("lime", "banana");
	console.log("mutated array from unshift method");
	console.log(fruits);

	function unshiftFruit(fruit1, fruit2){
		fruits.unshift(fruit1, fruit2);
	}
	unshiftFruit("grapes", "papaya");
	console.log(fruits)

//shift()
//removes an element at the beginning of an array AND returns the removed element.
//Syntax
	//arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from the shift method");
	console.log(fruits);

//splice()
//simultaneously removes elements from a specified index number and adds elements
//syntax:
	//arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

	fruits.splice(1, 2, "Calamansi", "Cherry");

	console.log("Mutated array from splice method: ");
	console.log(fruits);

	function spliceFruit(index, deleteCount, fruit){
		fruits.splice(index, deleteCount, fruit);
	}

	spliceFruit(1, 1, "avocado");
	spliceFruit(2, 1, "durian");
	console.log(fruits)

//sort()
//rearranges the array elements in alphanumeric order

//syntax
	//arrayName.sort()

	fruits.sort();
	console.log("Mutated array from sort method: ")
	console.log(fruits);

//reverse()
//reverses the order of array elements(Z-A)
//syntax
	//arrayName.reverse();

	fruits.reverse();
	console.log("Mutated array from reverse Method: ")
	console.log(fruits);

	//mini-group activity