const http = require("http");

const port = 4000

const app = http.createServer((request, response)=> {

	if(request.url == "/login"){
		response.writeHead(200,{"Content-type":"text/plain"})
		response.end("This is the login page.")
	}

	else{
		response.writeHead(404,{"Content-type":"text/plain"})
		response.end("Error 404. Route not found.")
	}

})
app.listen(port);
console.log("Server successfully connected!")
