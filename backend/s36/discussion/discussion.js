//[MongoDB Aggregation]
/*
	Going to join data from multiple documents
	Aggregation gives us access to manipulate, filter, and compute for results providing us with information to make necessary development decisions without having to create a Frontend application
*/

//db - market_db
//collection - fruits

//In mongosh, you can create a db. You can simply use - use <db>
//In mongosh, you can create a collection. You can simply use - db.createCollection(<collectionName>)

	//*create a market_db database and fruits collection in MongoDB Compass

	//Create documents to use for the discussion
	
	db.fruits.insertMany([
		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id: 1,
			onSale: true,
			origin:["Philippines","US"]
		},
		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id:2,
			onSale: true,
			origin: ["Philippines","Ecuador"]
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id:1,
			onSale: true,
			origin: ["US","China"]
		},
		{
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id:2,
			onSale: false,
			origin: ["Philippines","India"]
		}
	])

	//Use the aggregate method:
	/*
		Syntax:
			db.collectionName.aggregate([
				{ $match: {fieldA: value } },
				{ $group: {_id: "$fieldB"}, result: {operation}  }
			])

		The $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process
			Syntax
				{$match: {field: value} }
		
		The $group is used to group elements together and field-value pairs using the data from the grouped elements
			Syntax
				{$group: { id: "value", fieldresult: "valueResult" }}

	*/

		//Using both $match and $group along with aggregate will find for fruits that are onSale and will group the total amount of stocks per supplier

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
		])

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id:0} }
		])

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id:0} }
		])


		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total:-1} }
		])
		
		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total:1} }
		])

		db.fruits.aggregate([
			{$unwind: "$origin"}
		])
		//yields 8 separate result

		db.fruits.aggregate([
			{$unwind: "$origin"},
			{$group: {_id: "$origin", kinds: {$sum: 1}}}
		])