//controller
const Course = require("../models/Course");
const auth = require("../auth")

module.exports.addCourse = (req, res) =>{
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	return newCourse.save().then((course, err)=>{
		if(course){
			return res.send(true)
		}else{
			return res.send(false)
		}
	})
}

//Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//We will use the find() method to for our course model


module.exports.getAllCourses = (req, res) =>{

	return Course.find({}).then(result=>{
		return res.send(result)
	})
}

//Retrieve active courses

module.exports.getAllActiveCourses = (req, res) =>{
	return Course.find({isActive: true}).then(result=>{
		return res.send(result)
	})
}


//Retrieve specific course

module.exports.getCourse = (req, res) =>{
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result)
	})
}

//Update specific course

module.exports.updateCourse = (req, res) =>{

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) =>{{

		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	}})

}

//Activity

module.exports.archiveCourse = (req, res) =>{

	return Course.findByIdAndUpdate(req.params.courseId, {isActive: false}).then(result=>{
		return res.send(true)
	})
}


module.exports.activateCourse = (req, res) =>{

	return Course.findByIdAndUpdate(req.params.courseId, {isActive: true}).then(result=>{
		return res.send(true)
	})
}