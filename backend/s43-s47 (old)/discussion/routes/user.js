//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;


//Routing component
const router = express.Router()

//Routes - POST
//Check email
router.post("/checkEmail",(req, res) =>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication

router.post("/login",userController.loginUser)

//Activity
router.post("/details", verify, userController.getProfile)

//Route to enroll user to a course
router.post("/enroll", verify, userController.enroll)



//Export Route System
module.exports = router;


// things added to index.js
//	app.use("/users", userRoutes);
// const userRoutes = require("./routes/user")

//app.use("/courses", courseRoutes)
//const courseRoutes = require("./routes/course")