//Route

//Dependencies and Modules

const express = require("express");
const courseController = require("../controllers/course")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;

//Routing component
const router = express.Router()

//ROUTES

router.post("/", verify, verifyAdmin, courseController.addCourse)

//route for retrieving all courses
router.get("/all", courseController.getAllCourses)

//route for retrieving active courses
router.get("/", courseController.getAllActiveCourses)

//route for retrieving specific course
router.get("/:courseId", courseController.getCourse)

//route for editing specific course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse)



//Activity
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse)
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse)

//Export Route System
module.exports = router;