console.log("JS Objects!");

//[Objects]
/*
	- An object is a data type that is used to represent real world objects
	- Information stored in objects are represented in a "Key:value" pair
	- A "key" is also mostly referred to as a "property" of an object's creating complex data structure

*/
/*
	Syntax
	let objectName = {
		keyA: valueA,
		keyB: valueB

	}
*/
	let ninja = {
		name: "Naruto",
		village: "Konoha",
		occupation: "Hokage"
	}
	console.log("Result from creating objects using initializers/literal notation");
	console.log(ninja);
	console.log(typeof ninja);

let dog = {
	name: "Whitey",
	color: "white",
	breed: "Chihuahua"
}

//Creating objects using a constructor function

/*
	create a reusable function to create several objects that have the same data structure
	Instance - is a concrete occurance of any object which emphasizes on its distinct/unique identity of it
*/

/*
	function Object Name (keyA,keyB){
		this.keyA = keyA;
		this.keyB = keyB;

	}
*/

function Laptop(name,manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
/*
	"this" keyword allows up to assign a new object's properties by associating them with the values received from a constructor function's parameter
*/

//Instances

/*
	"new" operator creates an INSTANCE of an object 
	Object and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects

*/

let laptop1 = new Laptop('Lenovo',2022);
console.log('Result from creating objects using object constructor');
console.log(laptop1);

let myLaptop = new Laptop('Macbook Air',2020);
console.log('Result from creating objects using object constructor');
console.log(myLaptop);

/*
	invoke/call "Laptop" function instead of creating a new object instance
	returns undefined without the "new" operator because the "Laptop" function does not have a return statement

*/
let oldLaptop = Laptop('Portal R2E CCMC',1980);
console.log("result from creating instance without new keyword");
console.log(oldLaptop);

	//Mini Activity 1
	//create 3 more instances of our laptop constructor
	//log these 3 instances in the console

let activityLaptop1 = new Laptop("TUF", 2022);
console.log("result from creating instance without new keyword");
console.log(activityLaptop1);

let activityLaptop2 = new Laptop("ROG", 2021);
console.log('Result from creating objects using object constructor');
console.log(activityLaptop2);

let activityLaptop3 = new Laptop("AlienWare", 2023);
console.log('Result from creating objects using object constructor');
console.log(activityLaptop3);

// Create empty objects
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer)

//[Access Object Properties]

	//1. do notation
console.log('Result ' + myLaptop.name);
console.log('Result ' + myLaptop.manufactureDate)
	//2. square bracket notation
console.log('Result ' + myLaptop['name']);
console.log('Result ' + myLaptop['manufactureDate']);

//Access array objects
/*
	Accessing array elements can also be done using square brackets
	Accessing object properties using the square bracket notation, and array indexes can cause confusion
*/

let array = [laptop1, myLaptop];
//square bracket
	//may be confused for accessing array indexes
console.log(array[0]['name']);

	//differentiation between accessing arrays and objects properties
	//this tells us that array [0] is na object by using the dot notation
//dot notation
console.log(array[0].name);

//[Initialize, Add, Delete, Reassign Object Properties]

let car = {};

car.name = 'HondaCivic';
console.log("result from adding properties using dot notation:")
console.log(car);

//car.number = [1,2,3];
//console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
//console.log(car.manufacture date);
console.log(car);
console.log(car.manufactureDate);//undefined

//delete object properties
delete car['manufacture date'];
console.log('Result from deleting properties')
console.log(car);

//reassign object properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:')
console.log(car);

//[Object Methods]
/*
	A method is a function which is a property of an object
	Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

let person = {
	name: "Cardo",
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}

console.log(person);
console.log('Result of object methods:');
person.talk ();

//walk
person.walk = function(){
	console.log(this.name + ' walked a 25 steps forward!')
}
person.walk();


let friend = {
	firstName: 'Nami',
	lastName: 'Misko',
	address: {
		city: 'Tokyo',
		country:'Japan'
	},
	emails: ['nami@sea.com','namimisko@gmail.com'],
	introduce: function(){
		console.log('Hello! name is ' + this.firstName + ' ' + this.lastName);
	}
}
friend.introduce();


//[Real world application of objects]

	/*
	Scenario:
	create a game that would have several pokemon interact with each other
	every pokemon would have the same set of stats, properties, and functions
	*/

//use object literals

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled Target Pokemon!")
		console.log("Target Pokemon's health is now reduced to Target Pokemon Health")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);
myPokemon.faint();

//create an object constructor

function Pokemon(name, level){

	//Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
	}
	this.faint = function(){
		return target.name + " fainted"
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);

function foo(){
	return 10;
}

function bar(){
	return 20;
}

function baz(){
	return foo + bar;
}

console.log(baz())