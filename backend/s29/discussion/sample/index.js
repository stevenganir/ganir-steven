console.log("DOM Manipulation!");

console.log(document);//html document
console.log(document.querySelector('#clicker'));//button element

/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (obj) as long as it is inside the html tag (html element)
	- takes a string input that if formatted like CSS selector
	- can select elements regardless if the string is an id, class, or tag as long as the element is existing in the webpage
*/

/*
	Alternative methods that we use aside from querySelector in retrieving elements:
		document.getElementById()
		document.getElementByClassName()
		document.getElementByTagName()
*/


/*
	innerHTML - is a property of an element which considers all the children of the selected element as a string.

	The innerHTML property captures the HTML contents of an element.

	.value of the input text field
*/

/*
	event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)
	(event) can have a shorthand of (e)
	

	addEventListener - function that lets the webpage to listen to the events performed by the user
	- Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.
	-takes two arguments
	-string - the event to whcih the HTML element will listen, these are predetermined
	-function - executed by the element once the event (first argyment) is triggered
	Syntax:
	selectedElement.addEventListener('event', function);
			
*/



let counter = 0;


const clicker = document.querySelector('#clicker');
	
	clicker.addEventListener('click',()=>{
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times`);
	})


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


// const updateFullName = function(e){}

const updateFullName = (e) =>{

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}
	
	txtLastName.addEventListener('keyup', updateFullName);
	txtFirstName.addEventListener('keyup',updateFullName);




/*txtFirstName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

const labelFirstName = document.querySelector("#label-txt-first-name");

labelFirstName.addEventListener('mouseover',(e)=>{

	alert("You hovered the First Name Label!")

})
*/
/*
	Mini-Activity
	Make a keyup event where the values of the first name and last name will populate the span element with the id #span-full-name
	Send the output in the batch google chat MA thread

*/


