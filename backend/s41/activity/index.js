const express = require("express");
const mongoose = require('mongoose');

const app = express();
const port = 3001;


app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://steven:admin123@batch-297.mi0eika.mongodb.net/userDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema)



app.post("/signup", (req, res) => {
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.

	User.findOne({username: req.body.username}).then((result, err) => {

		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} 
		else {
			if(req.body.username != "" && req.body.password != ""){
				// Create a new task and save it to the database
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

				newUser.save().then((savedUser, saveErr) => {
					if(saveErr){
						return console.error(saveErr);
					} else {
						return res.status(201).send("New user registered");
					}
				})
			}
			else{
				return res.send("BOTH username and password must be provided")
			}
		}
	})
})

// Listen to the port, meaning, if the port is accessed, we will run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;