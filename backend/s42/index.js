//Setup a basic Express JS server
//Dependencies
let express = require("express");
let mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

//Server setup
const app = express();
const port = 4000;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Database connection (Connect to MongoDB)
mongoose.connect("mongodb+srv://steven:admin123@batch-297.mi0eika.mongodb.net/taskDB?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
//prompt
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"))
db.once("open", ()=>console.log("Connected to MongoDB Atlas."))

app.use("/tasks", taskRoute)
//http://localhost4000/tasks


//Server listening

if(require.main === module){
	app.listen(port, ()=>console.log(`Server running at port ${port}`));
}

module.exports = {app,mongoose};