/*
	npm init - used to initialize a new package.json file
	package.json - contains info about the project, dependencies, and other settings
	package-lock.json - locks the versions of all installed dependencies to its specific version
*/

const express = require("express")
const app = express();
const port = 3000;

//Middlewares
		//a software that provides common services and capabilities to application outside of what's offered by operating system.

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

//Allows our app to read json data
app.use(express.json())
//Allows our app to read data from forms
//this also allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))

app.get("/",(req, res)=>{
	res.send("Hello World")
})

app.get("/hello",(req, res)=>{
	res.send("Hello from /hello endpoint")
})

// app.post("/hello",(req, res)=>{
// 	res.send("Hello from post route")
// })

app.post("/hello",(req, res)=>{
	res.send(`Hello ${req.body.firstName} ${req.body.lastName}`)
})

app.put("/",(req, res)=>{
	res.send("Hello from put")
})

app.delete("/",(req, res)=>{
	res.send("Hello from delete")
})
//array will store objects when the "/signup" route is accessed
let users = []

// app.post("/signup",(req,res)=>{
// 	console.log(req.body)
// 	users.push(req.body)
// 	res.send(users)
// })

app.post("/signup",(req,res)=>{

	if(req.body.username != "" && req.body.password != ""){
		users.push(req.body)
		res.send(`The request has been successfully processed!`)
		console.log(users)
	}
	else{
		res.send(`Please input both username and password`)
	}
	
})

app.put("/change-password",(req,res)=>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`

			console.log(users)

			break;
		}
		else{
			message = `User does not exist`
		}
	}

	res.send(message);
})

//Activity


app.delete("/delete-user",(req, res)=>{

	console.log(users)

	let message;
	
	if(users.length>0){
			for(let i=0; i<users.length; i++){

				if(req.body.username == users[i].username){
					users.splice(i, 1);

					message = `User ${req.body.username} has been deleted.`;

					console.log(users);

					break;
				}
			}
			if(message === undefined){
				message = "User does not exist."
			}
	}

	else{
		message = "No users found."
	}

	res.send(message)
})

//End of Activity

if(require.main === module){
	app.listen(port, ()=>console.log(`Server running at port ${port}`))

}
module.exports = app;