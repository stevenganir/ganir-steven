console.log('h');

/*
	Non-mutator Methods
		-Area functions that do not modify or change an array after they are created
		-Do not manipulate the originial array performing various tasks such as:
			-returning elements from an array
			-combining arrays
			-printing the output
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//indexOf()
/*
	-Return the index number of the first matching element found in an array
	-If no match is found, the result will be -1.
*/

	let firstIndex = countries.indexOf("PH");
	console.log("Result of indexOf method: " + firstIndex);

	let invalidCountry = countries.indexOf("BR");
	console.log("Result of indexOf method: " + invalidCountry);

//lastIndexOf()
/*
	-Returns the index number of the last matching element found in the array
	-If no match is found, the result will be -1.
*/

	let lastIndex = countries.lastIndexOf("PH");
	console.log("Result of lastIndexOf method: " + lastIndex);

	let lastIndexStart = countries.lastIndexOf("PH", 6);//5
	// let lastIndexStart = countries.lastIndexOf("PH", 3);//1
	console.log("Result of lastIndexOf method: " + lastIndexStart);

//slice()
/*
	Portions/slices elements from an array AND returns a new array
*/

console.log(countries);

let slicedArrayA = countries.slice(2);
console.log("Result from slice methods:")
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice methods:")
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice methods:")
console.log(slicedArrayC);

//toString()
/*
	Returns an array as a string separated by commas
*/

let stringArray = countries.toString();
console.log("Result from toString method:")
console.log(stringArray);

let sampArr = [1, 2, 3];
console.log(sampArr.toString());

//concat()
/*
	combines two arrays and returns the combined result
*/

let taskArrayA = ["drink html", 'eat javascript'];
let taskArrayB = ["inhale CSS", 'breath mongodb'];
let taskArrayC = ["get git", 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ")
console.log(tasks);

console.log("Result from concat method: ")
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat("smell express", "thr react");
console.log("Result from concat method: ")
console.log(combinedTasks);

//join()
/*
	Returns an array asa string separated by specified separator (string data type)
*/

let users = ["John", "Jane", "Joe", "James"];
console.log(users.join());//default
console.log(users.join(""));
console.log(users.join(" - "));

/*
	Iterator Methods

	-These are loops designed to perform repetitive tasks on arrays
	-Array Iteration methods normally work with a function supplied as an argument
*/

//forEach()
/*
	-Similar to a for loop that iterates on each array element
	-For each item in the array, the ANONYMOUS FUNCTION passed in the forEach() method will be run.
	-The anonymous function is able to receive the current item being iterated or loop over by assigning a PARAMETER
	-Variable names for arrays are normally written in the plural form
	-It is a common practice to use a singular form of the array content for the parameter names used in array loops
	-forEach() does not return anything
*/

console.log(allTasks);

	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks = [];

	allTasks.forEach(function(task){

		//console.log(task);
		if(task.length>10){
			filteredTasks.push(task);
		}


	});

	console.log("Result of filteredTasks: ");
	console.log(filteredTasks);

//map()
/*
	
*/

	let numbers = [1,2 ,3 ,4,5];

	let numberMap = numbers.map(function(number){
		return number * number;

	})

	console.log("Original Array:");
	console.log(numbers);
	console.log("Result of map Array:");
	console.log(numberMap);

	let numberForEach = numbers.forEach(function(number){
		return number *number;
	})

	console.log(numberForEach);

//every()

/*
	Checks if all elements in an array meet a given condition
*/

	let allValid = numbers.every(function(number){
	return (number < 3);
	})

	console.log("Result of every method:");
	console.log(allValid);

//some()
/*
	Checks if at least one element in the array meets the given condition
*/

let someValid = numbers.some(function(number){
	return (number < 2);

})

console.log("Result of some method:");
	console.log(someValid);//true

//filter()
/*
	Returns a new array that contains elements which meets the given condition
	Returns an empty array if no elements were found
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
	})

console.log("Result of filter method:");
	console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})

console.log("Result of filter method:");
	console.log(nothingFound);

//includes()
	/*
		checks if the argument passed can be found in the array

	*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

/*
	Methods can be "chained" using them one after another
*/

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})
console.log(filteredProducts);

//reduce()
/*
	Evaluates elements from left to right and returns or reduces the array into a single value

	//"accumulator" parameter in the function stores the result for every iteration of the loop
	//"Current value" parameter is the current element in the array that is evaluated in eact iteration of the loop

	Process: 
	1. First element in the array is stored in the accumulator
	2. Second element in the array is stored in the current value
	3. an operation is performed on the 2 elements
	4. the loop repeats the whole step 1-3 until all elements have been worked on.
*/

console.log(numbers);//[1, 2, 3, 4, 5]
					//1 + 2 = 3
					//3 + 3 = 6
					//6 + 4 = 10
					//10 + 5 = 15

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("accumulator is " + x);
	console.log("current value is " + y);
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

console.log(reducedArray);

//reduce string arrays to string

let list = ["Hello", "From", "The", "Other", "Side"];

let reducedJoin = list.reduce(function(x, y){
	console.log(x);
	console.log(y);
	return x + ' ' + y;
})
console.log("Result of reduce methods: " + reducedJoin);